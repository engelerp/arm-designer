#version 330 core
layout (location = 0) in vec2 vpos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float rot_angle;

uniform vec2 offset;
uniform float zoffset;

void main()
{
    float x0 = cos(rot_angle) * vpos.x - sin(rot_angle) * vpos.y;
    float y0 = sin(rot_angle) * vpos.x + cos(rot_angle) * vpos.y;
    float x1 = x0 + offset.x;
    float y1 = y0 + offset.y;
    gl_Position = projection * view * model * vec4(x1, y1, zoffset, 1.0);
}