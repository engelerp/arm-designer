#pragma once
#include <vector>
#include <array>
#include <string>
#include <segment.hpp>

class Segments {
public:
	Segments();

	bool load(std::string filename);
	bool save(std::string filename) const;
	bool save_ac(std::string filename, std::vector<size_t> selection, std::vector<int> reps) const;
	void clear();
	void erase(size_t i);
	const std::vector<Segment>& get_segments() const;
	std::vector<Segment>& get_segments();
	size_t size() const;

	void split(size_t segment_i, size_t node1_i, size_t node2_i);

private:
	std::vector<Segment> segms_;
};
