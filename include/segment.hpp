#pragma once
#include <string>
#include <vector>
#include <array>

class Segment {
public:
	Segment(std::string filename);
	Segment(std::vector<std::array<float, 2> > pts);

	void save(std::string filename) const;
	bool save_sc(std::string filename, int reps) const;
	std::string get_autocad_script(int reps) const;
	void move_to(size_t i, const std::array<float, 2> pos);
	void insert(size_t i, const std::array<float, 2> pos);
	const std::vector<std::array<float, 2> >& nodes() const;
	void erase(const std::vector<std::array<float, 2> >& coords);
	void clear();

	std::vector<std::array<float, 2> > split(size_t i, size_t j);

private:
	/*Vector of points, pts[i] is connected to pts[i-1] and pts[i+1]*/
	std::vector<std::array<float, 2> > pts_;
};
