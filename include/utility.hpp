#pragma once
#include <cmath>
#include <string>
#include <vector>

float distance(float x0, float y0, float x1, float y1) {
	return std::sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
}

bool is_whitespace(const char& c) {
	return c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r';
}

std::vector<std::string> split_str_ws(std::string s) {
	size_t i = 0;
	std::vector<std::string> strings;
	while (i != s.size()) {
		std::string new_string = "";
		//skip leading whitespace
		while (i != s.size() && is_whitespace(s[i])) {
			++i;
		}
		if (i == s.size()) {
			return strings;
		}
		while (i != s.size() && !is_whitespace(s[i])) {
			new_string += s[i++];
		}
		strings.push_back(new_string);
	}
	return strings;
}