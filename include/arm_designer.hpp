#pragma once
#include <glad/glad.h>
#include <SDL.h>
#include <infrastructure.hpp>
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>
#include <segments.hpp>
//#include <undo_ops.hpp>
#include <render_parameters.hpp>
#include <gui_parameters.hpp>
#include <utility>
#include <tuple>

/*Window Resolution*/
#define WIDTH 1280
#define HEIGHT 1280
/*Debugging: Get VRAM info*/
#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049
/*Paths*/
/*HOME*/
//#define RESOURCEPATH "C:\\Users\\engel\\repos\\arm-designer\\resources\\"
/*ZYGOTE*/
#define RESOURCEPATH "C:\\Users\\Pascal\\repos\\arm-designer\\resources\\"
class ArmDesigner {
public:
	ArmDesigner();

	/*Run Application*/
	bool run();

private:
	/*Managers*/
	Infrastructure infra_;
	Segments segments_;
	//UndoOps undo_ops_;
	/*Parameter Containers*/
	RenderParameters render_params_;
	GuiParameters gui_params_;
	std::vector<std::pair<size_t, size_t> > selected_nodes_; //segment index, node index
	std::vector<std::array<float, 2> > selected_nodes_inipos_; //positions of selected nodes when dragging begins
	/*Measurement Mode*/
	std::array<float, 2> meas_origin_;
	float meas_dist_;
	bool meas_valid_ = false;
	/*Mouse info*/
	std::array<float, 2> mouse_press_loc_;


	/*Member Functions*/
	/*DEBUG Opengl*/
	void debug_opengl_() const;

	/*GUI Initialize*/
	void gui_init_();
	/*GUI Start Drawing*/
	void gui_new_frame_();
	/*GUI Draw Debug Window*/
	void gui_draw_debug_window_();
	/*GUI Drawn Controller Window*/
	void gui_draw_controller_window_();
	/*Draw Tooltip Window*/
	void gui_draw_tooltip_window_();
	/*GUI Draw Log Window*/
	void gui_draw_log_window_();
	/*GUI Finish Drawing*/
	void gui_finish_drawing_();

	/*RENDERER Initialize*/
	void renderer_init_();
	/*RENDERER Render*/
	void renderer_render_();

	/*INPUT handle*/
	bool input_handle_();

	/*RESET view*/
	void reset_view_();

	/*Find closest segment, node and distance*/
	std::tuple<size_t, size_t, float> find_closest_seg_node_dist_(const float x, const float y) const;
	/*Find closest segment, line and distance*/
	std::tuple<size_t, size_t, size_t, float> find_closest_seg_line_dist_(const float x, const float y) const;

	/*Translate mouse coords into real space coords*/
	std::array<float, 2> get_real_mouse_coords_() const;
};
