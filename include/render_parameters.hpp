#pragma once
#include <glad/glad.h>
#include <string>
#include <shader.hpp>

struct RenderParameters {
	glm::vec2 offset;
	float zoffset;
	float fov;

	GLuint vbo_segment_nodes;
	GLuint vao_segment_nodes;
	Shader shader_segment_lines;
	glm::vec3 color_segment_lines;
	glm::vec3 color_segment_points;
	glm::vec3 color_segment_rep_lines;
	glm::vec3 color_segment_rep_points;
	glm::vec3 color_segment_sel_lines;
	glm::vec3 color_segment_sel_points;
	glm::vec3 color_segment_clo_points;
	glm::vec3 color_segment_clo_lines;
	glm::vec3 color_segment_grayscale;
	glm::vec3 color_meas;
	glm::vec3 color_sketch;
	glm::vec3 color_sel_sketch;

	std::vector<float> grid_coords;
	GLuint vbo_grid_nodes;
	GLuint vao_grid_nodes;
	glm::vec3 color_grid_lines;

	std::vector<float> drum_coords;
	GLuint vbo_drum_nodes;
	GLuint vao_drum_nodes;
	glm::vec3 color_drum_lines;

	std::string resource_path;
};