#pragma once
#include <string>

enum class SEL_MODE : int {NONE=0, NODE, LINE, SEGMENT, MEASURE, SKETCH};

struct GuiParameters {
	/*Path from which the next segment is loaded*/
	char segment_load_path[128];
	char segment_save_path[128];
	char segment_save_sc_path[128];
	char segment_save_ac_path[128];

	/*Enable / Disable parameters*/
	bool enable_drum_contour;

	/*Segment Settings*/
	std::vector<bool> segments_shown;
	std::vector<int> segments_repetitions;
	std::vector<bool> segments_grayscale;

	/*Mouse Selection Mode*/
	SEL_MODE selection_mode = SEL_MODE::NONE;

	/*Closest Object*/
	bool mindist_en = false;
	float mindist_dist;
	size_t mindist_seg_i;
	size_t mindist_node_i;
	size_t mindist_node_i2; //used when selecting lines

	/*Log*/
	std::vector<std::string> log_messages;
};
