#define _USE_MATH_DEFINES
#include <segment.hpp>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <ios>
#include <algorithm>
#include <cmath>

Segment::Segment(std::string filename){
	std::ifstream infile(filename);
	if (!infile.is_open()) {
		throw("IO ERROR");
	}
	float x = 0.;
	float y = 0.;
	size_t count = 0;
	while (infile >> x && infile >> y) {
		++count;
		pts_.push_back({ x, y });
	}

#ifndef NDEBUG
	for (auto& arr : pts_) {
		std::cout << arr[0] << "\t" << arr[1] << "\t" << &arr[0] << "\t" << &arr[1] << std::endl;
	}
	std::cout << "Loaded Segment of " << count << " Nodes.\n";
#endif
	infile.close();
}

Segment::Segment(std::vector<std::array<float, 2> > pts): pts_(pts) {}

void Segment::save(std::string filename) const {
	std::ofstream outfile(filename, std::ios::out);
	if (!outfile.is_open()) {
		std::cout << "File " << filename << " can't be opened.\n" << std::endl;
		return;
		//std::exit(1);
	}
	else {
		for (auto arr : pts_) {
			outfile << std::to_string(arr[0]) << "\t" << std::to_string(arr[1]) << std::endl;
		}
	}
	outfile.close();
}

bool Segment::save_sc(std::string filename, int reps) const {
	std::ofstream outfile(filename, std::ios::out);
	if (!outfile.is_open()) {
		std::cout << "File " << filename << " can't be opened.\n" << std::endl;
		return false;
	}
	else {
		std::string printstr = "3d = true\npolyline = true\nfit = false\nfittol = 0.0001\n\n";
		for (size_t k = 0; k < reps; ++k) {
			float rot_angle = k * 2. * M_PI / reps;
			for (auto n : pts_) {
				float cos = std::cos(rot_angle);
				float sin = std::sin(rot_angle);
				float x = cos * n[0] - sin * n[1];
				float y = sin * n[0] + cos * n[1];
				printstr += std::to_string(x) + " " + std::to_string(y) + " 0\n";
			}
			printstr += "\n";
		}
		outfile << printstr;
		outfile.close();
		return true;
	}
}

std::string Segment::get_autocad_script(int reps) const {
	std::string printstr = "";
	float multiplier = 1000.f; //dxf/gds units are weird, this is what was used in previous samples
	for(size_t k = 0; k < reps; ++k){
		float rot_angle = k * 2. * M_PI / reps;
		for(size_t i = 0; i < pts_.size(); ++i){
			auto n = pts_[i];
			auto m = pts_[i];
			//closed curve
			if(i+1 < pts_.size()){
				m = pts_[i+1];
			}
			else{
				m = pts_[0];
			}
			float cos = std::cos(rot_angle);
			float sin = std::sin(rot_angle);
			float x0 = cos * n[0] - sin * n[1];
			float y0 = sin * n[0] + cos * n[1];
			float x1 = cos * m[0] - sin * m[1];
			float y1 = sin * m[0] + cos * m[1];
			printstr += "line\n";
			printstr += std::to_string(multiplier * x0) + "," + std::to_string(multiplier * y0) + "\n";
			printstr += std::to_string(multiplier * x1) + "," + std::to_string(multiplier * y1) + "\n";
			printstr += "\n"; //stop line drawing. potentially could just chain a single line per hole.
		}
	}
	return printstr;
}

void Segment::move_to(size_t i, const std::array<float, 2> pos) {
	pts_[i] = pos;
}

void Segment::insert(size_t i, const std::array<float, 2> pos) {
	pts_.insert(pts_.begin() + i, pos);
}

const std::vector<std::array<float, 2> >& Segment::nodes() const{
	return pts_;
}

void Segment::erase(const std::vector<std::array<float, 2> >& coords) {
	pts_.erase(std::remove_if(pts_.begin(), pts_.end(),
		[&coords](const std::array<float, 2>& xy) {
			for (auto coord : coords) {
				if (xy[0] == coord[0] && xy[1] == coord[1]) {
					return true;
				}
			}
			return false;
		}), pts_.end());
}

void Segment::clear() {
	pts_.clear();
}

std::vector<std::array<float, 2> > Segment::split(size_t i, size_t j) {
	std::vector<std::array<float, 2> > retvec(pts_.begin() + std::min(i, j) + 1, pts_.begin() + std::max(i, j));
	std::vector<std::array<float, 2> > new_pts(pts_.begin(), pts_.begin() + std::min(i, j) + 1);
	for (size_t k = std::max(i, j); k < pts_.size(); ++k) {
		new_pts.push_back(pts_[k]);
	}
	pts_ = new_pts;
	return retvec;
}
