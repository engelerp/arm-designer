#include <arm_designer.hpp>
#include <iostream>


int main(int argc, char** argv) {
	std::cout << "Program is Starting\n";

	ArmDesigner arm_designer;

	arm_designer.run();

	std::cout << "Program exited.\n";

	return 0;
}