#include <segments.hpp>
#include <segment.hpp>
#include <fstream>
#include <iostream>
#include <ios>
//#include <utility.hpp>

Segments::Segments() {
	std::vector<std::array<float, 2> > sketch_segm;
	segms_.push_back(sketch_segm); //the 0th segment is always the sketch segment
}

bool Segments::load(std::string filename) {
	//check if we have coordinates only, or a SpaceClaim compatible text file
	std::ifstream infile(filename);
	bool is_sc = false;
	std::string firstline;
	std::getline(infile, firstline);
	if (firstline == "3d = true") {
		std::cout << "Detected SpaceClaim Input File" << std::endl;
		is_sc = true;
	}
	if (!is_sc) {//Just Coordinates
		infile.close();
		Segment seg(filename);
		segms_.push_back(seg);
	}
	else {//SpaceClaim File
		infile.seekg(0);
		std::string line;
		//skip header
		std::getline(infile, line);
		while (line != "") {
			std::getline(infile, line);
		}
		std::vector<std::array<float, 2> > nodes;
		while (std::getline(infile, line)) {
			if (line == "") {
				break;
			}
			size_t index = 0;
			float x = std::stof(std::string(line.begin(), line.end()), &index);
			float y = std::stof(std::string(line.begin() + index, line.end()));
			nodes.push_back({ x,y });
			//auto strings = split_str_ws(line);
			//float x = std::stof(strings[0]);
			//float y = std::stof(strings[1]);
		}
		Segment seg(nodes);
		segms_.push_back(seg);
	}
	return true;
}

bool Segments::save(std::string filename) const {
	for (size_t i = 0; i < segms_.size(); ++i) {
		std::string filename_i = filename + std::to_string(i);
		segms_[i].save(filename_i);
	}
	return true;
}

bool Segments::save_ac(std::string filename, std::vector<size_t> selection, std::vector<int> reps) const{
	std::ofstream outfile(filename, std::ios::out);
	if(!outfile.is_open()){
		std::cout << "File " << filename << " can't be opened.\n" << std::endl;
		return false;
	}
	else{
		for (size_t i = 0; i < selection.size(); ++i) {
			outfile << segms_[selection[i]].get_autocad_script(reps[i]);
		}
		outfile.close();
		return true;
	}
}

void Segments::clear() {
	segms_.clear();
	//re-initialize the sketching segment
	std::vector<std::array<float, 2> > sketch_segm;
	segms_.push_back(sketch_segm);
}

void Segments::erase(size_t i) {
	if (i == 0) {
		//don't allow deletion of sketching segment
		return;
	}
	segms_.erase(segms_.begin() + i);
}

const std::vector<Segment>& Segments::get_segments() const{
	return segms_;
}

std::vector<Segment>& Segments::get_segments() {
	return segms_;
}

size_t Segments::size() const {
	return segms_.size();
}

void Segments::split(size_t segment_i, size_t node1_i, size_t node2_i) {
	segms_.push_back(Segment(segms_[segment_i].split(node1_i, node2_i)));

}
