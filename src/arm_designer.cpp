#include <arm_designer.hpp>
#include <iostream>
#include <cmath>
#include <utility.hpp>
#include <cassert>
#include <algorithm>//std::sort, std::for_each

ArmDesigner::ArmDesigner() {
		infra_.init("Arm Designer", WIDTH, HEIGHT);
		gui_init_();
		renderer_init_();
		gui_params_.segments_shown.push_back(true);
		gui_params_.segments_repetitions.push_back(1);
		gui_params_.segments_grayscale.push_back(false);
}

/*Run application*/
bool ArmDesigner::run(){
	while(true){
		if (!input_handle_()) {
			break;
		}



		renderer_render_();



		gui_new_frame_();
		gui_draw_debug_window_();
		gui_draw_controller_window_();
		gui_draw_tooltip_window_();
		gui_draw_log_window_();
		gui_finish_drawing_();

		SDL_GL_SwapWindow(infra_.window());

#ifndef NDEBUG
		debug_opengl_();
#endif

	}
	return true;
}

/*DEBUG Opengl*/
void ArmDesigner::debug_opengl_() const {
	GLint err = glGetError();
	if (err != GL_NO_ERROR) {
		std::cout << "Error Code: " << err << std::endl;
		std::cout << "GL_NO_ERROR: " << GL_NO_ERROR << std::endl;
		std::cout << "GL_INVALID_ENUM: " << GL_INVALID_ENUM << std::endl;
		std::cout << "GL_INVALID_VALUE: " << GL_INVALID_VALUE << std::endl;
		std::cout << "GL_INVALID_OPERATION: " << GL_INVALID_OPERATION << std::endl;
		std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION: " << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
		std::cout << "GL_OUT_OF_MEMORY: " << GL_OUT_OF_MEMORY << std::endl;
	}
}
/*GUI initialize*/
void ArmDesigner::gui_init_() {
	gui_params_.log_messages.push_back("Initializing GUI");
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForOpenGL(infra_.window(), infra_.context());
	const char* glsl_version = "#version 330";
	ImGui_ImplOpenGL3_Init(glsl_version);

	gui_params_.enable_drum_contour = true;
}
/*GUI Start Drawing*/
void ArmDesigner::gui_new_frame_() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(infra_.window());
	ImGui::NewFrame();
}
/*GUI Draw Debug Window*/
void ArmDesigner::gui_draw_debug_window_() {
	ImGui::Begin("Developer", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0 / double(ImGui::GetIO().Framerate), double(ImGui::GetIO().Framerate));
	GLint total_mem_kb = 0;
	glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &total_mem_kb);
	ImGui::Text("Total VRAM: %.3f MB", static_cast<float>(total_mem_kb) / 1000.f);
	GLint cur_avail_mem_kb = 0;
	glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &cur_avail_mem_kb);
	ImGui::Text("Available VRAM: %.3f MB", static_cast<float>(cur_avail_mem_kb) / 1000.f);
	ImGui::Text("Used VRAM: %.3f MB", static_cast<float>(total_mem_kb - cur_avail_mem_kb) / 1000.f);
	if (ImGui::GetIO().MousePos[0] < -1000000) {
		ImGui::Text("Mouse Coordinates: (N/A, N/A)");
	}
	else {
		ImGui::Text("Mouse Coordinates: (%5.f, %5.f)", ImGui::GetIO().MousePos[0], ImGui::GetIO().MousePos[1]);

		float mouse_x = ImGui::GetIO().MousePos[0] / static_cast<float>(WIDTH);
		float mouse_y = (HEIGHT - ImGui::GetIO().MousePos[1]) / static_cast<float>(HEIGHT);
		float scale = std::abs(render_params_.zoffset) * 2.f * std::tan(glm::radians(render_params_.fov / 2.f));
		ImGui::Text("Real Mouse Coordinates: (%.6f, %.6f", -(render_params_.offset.x - (mouse_x - 0.5) * scale), -(render_params_.offset.y - (mouse_y - 0.5) * scale));
	}
	ImGui::Text("FOV: %1.f", render_params_.fov);
	ImGui::Text("Horizontal Distance: %.1f um", std::abs(render_params_.zoffset) * 2.f * 1000.f * std::tan(glm::radians(render_params_.fov/2.f)));
	ImGui::Text("Center Coordinates: (%.3f, %.3f)", render_params_.offset.x, render_params_.offset.y);
	if (gui_params_.selection_mode == SEL_MODE::NODE && gui_params_.mindist_en) {
		ImGui::Text("Closest Node: Segment %u, Node %u, Distance %.4fmm", gui_params_.mindist_seg_i, gui_params_.mindist_node_i, gui_params_.mindist_dist);
	}
	else if (gui_params_.selection_mode == SEL_MODE::LINE && gui_params_.mindist_en) {
		ImGui::Text("Closest Line: Segment %u, Node1 %u, Node2 %u, Distance %.4fmm", gui_params_.mindist_seg_i, gui_params_.mindist_node_i, gui_params_.mindist_node_i2, gui_params_.mindist_dist);
	}
	else if (gui_params_.selection_mode == SEL_MODE::SEGMENT && gui_params_.mindist_en) {
		ImGui::Text("Not Implemented.");
	}
	else if (gui_params_.selection_mode == SEL_MODE::NONE || !gui_params_.mindist_en) {
		ImGui::Text("Closest: Disabled or none found.");
	}
	//calculate number of current nodes
	size_t physical_nodes = 0;
	size_t drawn_nodes = 0;
	for (size_t i = 0; i < segments_.size(); ++i) {
		physical_nodes += segments_.get_segments()[i].nodes().size();
		if (gui_params_.segments_shown[i]) {
			drawn_nodes += segments_.get_segments()[i].nodes().size() * gui_params_.segments_repetitions[i];
		}
	}
	ImGui::Text("Physical Nodes: %i", physical_nodes);
	ImGui::Text("Drawn Nodes: %i", drawn_nodes);
	for (auto n : selected_nodes_) {
		ImGui::Text("Selected: Segment %u, Node %u", n.first, n.second);
	}
	ImGui::End();
}
/*GUI Draw Controller Window*/
void ArmDesigner::gui_draw_controller_window_() {
	SEL_MODE last_sel_mode = gui_params_.selection_mode; //to track if selection mode changes this frame
	ImGui::Begin("Controller", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	if (ImGui::Button("Load")) {
		try {
			segments_.load(RESOURCEPATH + std::string("designs\\") + gui_params_.segment_load_path);
			gui_params_.log_messages.push_back("Loading from file " + std::string(RESOURCEPATH) + std::string("designs\\") + gui_params_.segment_load_path);
		}
		catch (...) {
			gui_params_.log_messages.push_back("Loading from file " + std::string(RESOURCEPATH) + std::string("designs\\") + gui_params_.segment_load_path + " failed.");
		}
		gui_params_.segments_shown.push_back(true);
		gui_params_.segments_repetitions.push_back(1);
		gui_params_.segments_grayscale.push_back(false);
	}
	ImGui::SameLine();
	ImGui::InputText("Filename to Load", gui_params_.segment_load_path, IM_ARRAYSIZE(gui_params_.segment_load_path));
	if (ImGui::Button("Save")) {
		gui_params_.log_messages.push_back("Saving Segment " + std::to_string(selected_nodes_[0].first) + " to " + RESOURCEPATH + std::string("designs\\saved\\") + gui_params_.segment_save_path);
		segments_.save(RESOURCEPATH + std::string("designs\\saved\\") + gui_params_.segment_save_path);
	}
	ImGui::SameLine();
	ImGui::InputText("Filename to Save", gui_params_.segment_save_path, IM_ARRAYSIZE(gui_params_.segment_save_path));
	if (ImGui::Button("Save SC")) {
		if (gui_params_.selection_mode == SEL_MODE::SEGMENT && selected_nodes_.size() == 1) {
			gui_params_.log_messages.push_back("Saving SC Segment " + std::to_string(selected_nodes_[0].first) + " to " + RESOURCEPATH + std::string("designs\\saved\\") + gui_params_.segment_save_sc_path);
			segments_.get_segments()[selected_nodes_[0].first].save_sc(RESOURCEPATH + std::string("designs\\saved\\") + gui_params_.segment_save_sc_path, gui_params_.segments_repetitions[selected_nodes_[0].first]);
		}
		else {
			gui_params_.log_messages.push_back("Saving Failure: Select one Segment to save.");
		}
	}
	ImGui::SameLine();
	ImGui::InputText("Filename to Save SC", gui_params_.segment_save_sc_path, IM_ARRAYSIZE(gui_params_.segment_save_sc_path));
	if(ImGui::Button("Save AC")){
		if(gui_params_.selection_mode == SEL_MODE::SEGMENT && selected_nodes_.size() > 0) {
			//get selected segment indices and reps
			std::vector<size_t> selection_indices;
			std::vector<int> selection_reps;
			std::for_each(selected_nodes_.begin(), selected_nodes_.end(), [&](const auto& n){selection_indices.push_back(n.first);});
			std::for_each(selection_indices.begin(), selection_indices.end(), [&](const auto& i){selection_reps.push_back(gui_params_.segments_repetitions[i]);});
			//log
			std::string logstring = std::string("Saving AutoCAD script ") + std::string(RESOURCEPATH) + std::string("designs\\saved\\") + gui_params_.segment_save_sc_path + std::string(" for segments ");
			std::for_each(selection_indices.begin(), selection_indices.end(), [&](const auto& i){logstring += std::to_string(i) + " ";});
			//save
			segments_.save_ac(RESOURCEPATH + std::string("designs\\autocad_scripts\\") + gui_params_.segment_save_ac_path, selection_indices, selection_reps);
		}
		else{
			gui_params_.log_messages.push_back("Saving AC Failure: Select one or more Segments to save.");
		}
	}
	ImGui::SameLine();
	ImGui::InputText("Filename to Save AC", gui_params_.segment_save_ac_path, IM_ARRAYSIZE(gui_params_.segment_save_ac_path));
	if (ImGui::Button("Clear Segments")) {
		segments_.clear();
		selected_nodes_.clear();
		selected_nodes_inipos_.clear();
		gui_params_.segments_shown.clear();
		gui_params_.segments_repetitions.clear();
		gui_params_.segments_grayscale.clear();
		gui_params_.segments_shown.push_back(true);
		gui_params_.segments_repetitions.push_back(1);
		gui_params_.segments_grayscale.push_back(false);
	}
	if (ImGui::Button("Reset View")) {
		reset_view_();
	}
	ImGui::Text("Selection Mode:");
	ImGui::SameLine();
	ImGui::RadioButton("None", (int*) &gui_params_.selection_mode, 0);
	ImGui::SameLine();
	ImGui::RadioButton("Node", (int*)&gui_params_.selection_mode, 1);
	ImGui::SameLine();
	ImGui::RadioButton("Line", (int*)&gui_params_.selection_mode, 2);
	ImGui::SameLine();
	ImGui::RadioButton("Segment", (int*)&gui_params_.selection_mode, 3);
	ImGui::SameLine();
	ImGui::RadioButton("Measure", (int*)&gui_params_.selection_mode, 4);
	ImGui::SameLine();
	ImGui::RadioButton("Sketch", (int*)&gui_params_.selection_mode, 5);
	ImGui::Checkbox("Draw Drum Contour", &gui_params_.enable_drum_contour);
	ImGui::Text("Segment Info");
	for (size_t i = 0; i < segments_.size(); ++i) {
		int reps = gui_params_.segments_repetitions[i];
		if (i != 0) {
			ImGui::Text("Segment %i", i);
		}
		else {
			ImGui::Text("Sketching Segment %i", i);
		}
		std::string name = std::string("Segment " + std::to_string(i) + " reps");
		ImGui::InputInt(name.c_str(), &reps);
		gui_params_.segments_repetitions[i] = reps;
		/*Bug: Cannot directly address entry in a vector of booleans*/
		bool shown = gui_params_.segments_shown[i];
		name = std::string("Segment " + std::to_string(i) + " shown");
		ImGui::Checkbox(name.c_str(), &shown);
		gui_params_.segments_shown[i] = shown;
		bool grayscale = gui_params_.segments_grayscale[i];
		name = std::string("Segment " + std::to_string(i) + " grayscale");
		ImGui::Checkbox(name.c_str(), &grayscale);
		gui_params_.segments_grayscale[i] = grayscale;
	}
	ImGui::End();
	if (last_sel_mode != gui_params_.selection_mode) {//selection mode changed
		gui_params_.log_messages.push_back("Mode switched from " + std::to_string((int)last_sel_mode) + " to " + std::to_string((int)gui_params_.selection_mode));
		selected_nodes_.clear();
		gui_params_.mindist_en = false;
		meas_valid_ = false;
	}
}
/*Draw Tooltip Window*/
void ArmDesigner::gui_draw_tooltip_window_() {
	if (gui_params_.selection_mode == SEL_MODE::MEASURE && meas_valid_) {
		auto mousepos = ImGui::GetMousePos();
		mousepos[0] += 30;
		ImGui::SetNextWindowPos(mousepos);
		auto real_mousepos = get_real_mouse_coords_();
		std::string name = "Tooltip";
		ImGui::Begin(name.c_str());
		ImGui::Text("Distance: %.3fum", meas_dist_ * 1000.f);
		ImGui::Text("Origin: (%.3f, %.3f)", meas_origin_[0], meas_origin_[1]);
		ImGui::Text("Probe: (%.3f, %.3f)", real_mousepos[0], real_mousepos[1]);
		ImGui::End();
	}
}
/*GUI Draw Log Window*/
void ArmDesigner::gui_draw_log_window_() {
	ImGui::SetNextWindowSize(ImVec2(700, 700));
	ImGui::Begin("Log", nullptr);
	for (auto s : gui_params_.log_messages) {
		ImGui::Text(s.c_str());
		ImGui::SetScrollHere(1.f);
	}
	ImGui::End();
}
/*GUI Finish Drawing*/
void ArmDesigner::gui_finish_drawing_() {
	ImGui::Render();
	glViewport(0, 0, WIDTH, HEIGHT);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/*RENDERER Init*/
void ArmDesigner::renderer_init_() {
	gui_params_.log_messages.push_back("Initializing Renderer");
	render_params_.resource_path = RESOURCEPATH;

	/*Segment objects*/
	glGenBuffers(1, &render_params_.vbo_segment_nodes);
	glGenVertexArrays(1, &render_params_.vao_segment_nodes);
	glBindVertexArray(render_params_.vao_segment_nodes);
	glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_segment_nodes);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), static_cast<void*>(0));
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*Grid objects*/
	for (size_t i = 0; i < 201; ++i) {
		render_params_.grid_coords.push_back(-1.f + i * 0.01);
		render_params_.grid_coords.push_back(-1.f);
		render_params_.grid_coords.push_back(-1.f + i * 0.01);
		render_params_.grid_coords.push_back(1.f);
		render_params_.grid_coords.push_back(-1.f);
		render_params_.grid_coords.push_back(-1.f + i * 0.01);
		render_params_.grid_coords.push_back(1.f);
		render_params_.grid_coords.push_back(-1.f + i * 0.01);
	}
	glGenBuffers(1, &render_params_.vbo_grid_nodes);
	glGenVertexArrays(1, &render_params_.vao_grid_nodes);
	glBindVertexArray(render_params_.vao_grid_nodes);
	glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_grid_nodes);
	glBufferData(GL_ARRAY_BUFFER, render_params_.grid_coords.size() * sizeof(float), render_params_.grid_coords.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), static_cast<void*>(0));
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*Drum contour objects*/
	for (size_t i = 0; i < 365; ++i) {
		render_params_.drum_coords.push_back(1.05 * 0.5 * std::cos(2.f * M_PI * static_cast<float>(i) / 365.f));
		render_params_.drum_coords.push_back(1.05 * 0.5 * std::sin(2.f * M_PI * static_cast<float>(i) / 365.f));
	}
	glGenBuffers(1, &render_params_.vbo_drum_nodes);
	glGenVertexArrays(1, &render_params_.vao_drum_nodes);
	glBindVertexArray(render_params_.vao_drum_nodes);
	glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_drum_nodes);
	glBufferData(GL_ARRAY_BUFFER, render_params_.drum_coords.size() * sizeof(float), render_params_.drum_coords.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), static_cast<void*>(0));
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);



	/*Line shader*/
	std::string vertpath = render_params_.resource_path + "shaders\\segment_line.vert";
	std::string fragpath = render_params_.resource_path + "shaders\\segment_line.frag";
	render_params_.shader_segment_lines = Shader(vertpath.c_str(), fragpath.c_str());
	render_params_.shader_segment_lines.use();
	glm::mat4 unity(1.f);
	render_params_.shader_segment_lines.setMat4("view", unity);
	render_params_.shader_segment_lines.setMat4("model", unity);
	render_params_.fov = 60.f;
	glm::mat4 projection = glm::perspective(glm::radians(render_params_.fov), 1.f, 0.f, 100.f);
	render_params_.shader_segment_lines.setMat4("projection", projection);
	render_params_.offset = glm::vec2(0.f, 0.f);
	render_params_.zoffset = -1.f;
	render_params_.shader_segment_lines.setFloat("zoffset", render_params_.zoffset);
	render_params_.shader_segment_lines.setVec2("offset", render_params_.offset);
	float rot_angle = 0.f;
	render_params_.shader_segment_lines.setFloat("rot_angle", rot_angle);


	render_params_.color_segment_lines = glm::vec3(0.f, 1.f, 0.f);
	render_params_.color_segment_points = glm::vec3(1.f, 1.f, 1.f);
	render_params_.color_segment_rep_lines = glm::vec3(0.f, 0.3f, 0.f);
	render_params_.color_segment_rep_points = glm::vec3(0.3f, 0.3f, 0.3f);
	render_params_.color_segment_sel_lines = glm::vec3(1.f, 1.f, 0.f);
	render_params_.color_segment_sel_points = glm::vec3(1.f, 1.f, 0.f);
	render_params_.color_segment_clo_lines = glm::vec3(1.f, 1.f, 1.f);
	render_params_.color_segment_clo_points = glm::vec3(1.f, 1.f, 1.f);
	render_params_.color_grid_lines = glm::vec3(0.25f, 0.25f, 0.25f);
	render_params_.color_drum_lines = glm::vec3(0.7f, 0.0f, 0.0f);
	render_params_.color_segment_grayscale = glm::vec3(0.3f, 0.3f, 0.3f);
	render_params_.color_meas = glm::vec3(1.f, 0.6f, 1.f);
	render_params_.color_sketch = glm::vec3(0.01f, 0.45f, 0.99f);
	render_params_.color_sel_sketch = glm::vec3(0.01f, 0.56f, 0.99f);




}
/*RENDERER Render*/
void ArmDesigner::renderer_render_() {
	glClearColor(0.f, 0.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	/*Update zoom*/
	glm::mat4 projection = glm::perspective(glm::radians(render_params_.fov), 1.f, 0.f, 100.f);

	//first we render the grid, it's in the background
	glLineWidth(0.3f);
	render_params_.shader_segment_lines.use();
	render_params_.shader_segment_lines.setVec3("color", render_params_.color_grid_lines);
	render_params_.shader_segment_lines.setMat4("projection", projection);
	render_params_.shader_segment_lines.setVec2("offset", render_params_.offset);
	glBindVertexArray(render_params_.vao_grid_nodes);
	glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_grid_nodes);
	glDrawArrays(GL_LINES, 0, render_params_.grid_coords.size() / 2);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//next draw drum contour if enabled
	if (gui_params_.enable_drum_contour) {
		glLineWidth(2.f);
		render_params_.shader_segment_lines.use();
		render_params_.shader_segment_lines.setVec3("color", render_params_.color_drum_lines);
		glBindVertexArray(render_params_.vao_drum_nodes);
		glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_drum_nodes);
		glDrawArrays(GL_LINE_LOOP, 0, render_params_.drum_coords.size() / 2);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//now draw all segments (note that the sketch segment 0 is drawn differently)
	auto segs = segments_.get_segments();
	for (size_t i = 0; i < segs.size(); ++i) {
		if (!gui_params_.segments_shown[i]) {
			continue;
		}
		auto seg = segs[i];
		auto nodes = seg.nodes();
		render_params_.shader_segment_lines.use();
		glBindVertexArray(render_params_.vao_segment_nodes);
		glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_segment_nodes);
		glBufferData(GL_ARRAY_BUFFER, nodes.size() * 2 * sizeof(float), nodes.data(), GL_STATIC_DRAW);
		for (size_t k = 0; k < gui_params_.segments_repetitions[i]; ++k) {
			float rot_angle = k * 2. * M_PI / gui_params_.segments_repetitions[i];
			render_params_.shader_segment_lines.setFloat("rot_angle", rot_angle);
			glLineWidth(1.f);
			if (k == 0) {
				if (gui_params_.segments_grayscale[i]) {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_grayscale);
				}
				else {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_lines);
				}
			}
			else {
				if (gui_params_.segments_grayscale[i]) {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_grayscale);
				}
				else {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_rep_lines);
				}
			}
			if (i != 0) {
				glDrawArrays(GL_LINE_STRIP, 0, nodes.size());
			}
			glPointSize(4.f);
			if (k == 0) {
				if (gui_params_.segments_grayscale[i]) {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_grayscale);
				}
				else if (i == 0) {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_sketch);
				}
				else {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_points);
				}
			}
			else {
				if (gui_params_.segments_grayscale[i]) {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_grayscale);
				}
				else {
					render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_rep_points);
				}
			}
			glDrawArrays(GL_POINTS, 0, nodes.size());
		}
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		float rot_angle = 0;
		render_params_.shader_segment_lines.setFloat("rot_angle", rot_angle);
	}

	//Now we draw the selected nodes
	if (selected_nodes_.size() > 0) {
		//check if all selected nodes are sketch nodes
		bool sketch_only = true;
		for (auto n : selected_nodes_) {
			if (n.first != 0) {
				sketch_only = false;
				break;
			}
		}
		render_params_.shader_segment_lines.use();
		glBindVertexArray(render_params_.vao_segment_nodes);
		glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_segment_nodes);
		//collect coordinates
		std::vector<std::array<float,2> > coords;
		if (gui_params_.selection_mode == SEL_MODE::NODE) {
			coords.reserve(2 * selected_nodes_.size());
			for (auto n : selected_nodes_) {
				coords.push_back(segments_.get_segments()[n.first].nodes()[n.second]);
			}
			glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * coords.size(), coords.data(), GL_STATIC_DRAW);
			if (sketch_only) {
				render_params_.shader_segment_lines.setVec3("color", render_params_.color_sel_sketch);
			}
			else {
				render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_sel_points);
			}
			glPointSize(7.f);
			glDrawArrays(GL_POINTS, 0, coords.size());
		}
		else if (gui_params_.selection_mode == SEL_MODE::LINE) {
			coords.reserve(4 * selected_nodes_.size());
			for (auto n : selected_nodes_) {
				coords.push_back(segments_.get_segments()[n.first].nodes()[n.second]);
				coords.push_back(segments_.get_segments()[n.first].nodes()[n.second+1]);
			}
			glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * coords.size(), coords.data(), GL_STATIC_DRAW);
			render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_sel_lines);
			glLineWidth(2.f);
			glDrawArrays(GL_LINES, 0, coords.size());
		}
		else if (gui_params_.selection_mode == SEL_MODE::SEGMENT) {
			coords.reserve(100);//typical, extracting actual maximum is probably more expensive
			for (auto n : selected_nodes_) {
				coords.clear();
				auto nodes = segs[n.first].nodes();
				for (auto m : nodes) {
					coords.push_back(m);
				}
				glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * coords.size(), coords.data(), GL_STATIC_DRAW);
				render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_sel_lines);
				glLineWidth(2.f);
				glDrawArrays(GL_LINE_STRIP, 0, coords.size());
				render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_sel_points);
				glPointSize(7.f);
				glDrawArrays(GL_POINTS, 0, coords.size());
			}
		}
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//Now we draw the close nodes/lines
	if (gui_params_.mindist_en) {
		render_params_.shader_segment_lines.use();
		glBindVertexArray(render_params_.vao_segment_nodes);
		glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_segment_nodes);
		if (gui_params_.selection_mode == SEL_MODE::NODE) {
			auto nodes = segments_.get_segments()[gui_params_.mindist_seg_i].nodes();
			glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), nodes[gui_params_.mindist_node_i].data(), GL_STATIC_DRAW);
			render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_clo_points);
			glPointSize(7.f);
			glDrawArrays(GL_POINTS, 0, 1);
		}
		else if (gui_params_.selection_mode == SEL_MODE::LINE) {
			auto nodes = segments_.get_segments()[gui_params_.mindist_seg_i].nodes();
			glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(float), nodes[gui_params_.mindist_node_i].data(), GL_STATIC_DRAW);
			render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_clo_lines);
			glLineWidth(2.f);
			glDrawArrays(GL_LINES, 0, 2);
		}
		else if (gui_params_.selection_mode == SEL_MODE::SEGMENT) {
			std::vector<std::array<float, 2> > coords;
			coords.reserve(100);//typical, extracting actual maximum is probably more expensive
			coords.clear();
			auto nodes = segs[gui_params_.mindist_seg_i].nodes();
			for (auto m : nodes) {
				coords.push_back(m);
			}
			glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * coords.size(), coords.data(), GL_STATIC_DRAW);
			render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_clo_lines);
			glLineWidth(2.f);
			glDrawArrays(GL_LINE_STRIP, 0, coords.size());
			render_params_.shader_segment_lines.setVec3("color", render_params_.color_segment_clo_points);
			glPointSize(7.f);
			glDrawArrays(GL_POINTS, 0, coords.size());
		}
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//Now draw measurements
	if (gui_params_.selection_mode == SEL_MODE::MEASURE && meas_valid_) {
		render_params_.shader_segment_lines.use();
		render_params_.shader_segment_lines.setVec3("color", render_params_.color_meas);
		auto mouse_coords = get_real_mouse_coords_();
		std::array<float, 4> nodes{ mouse_coords[0], mouse_coords[1], meas_origin_[0], meas_origin_[1] };
		glBindVertexArray(render_params_.vao_segment_nodes);
		glBindBuffer(GL_ARRAY_BUFFER, render_params_.vbo_segment_nodes);
		glBufferData(GL_ARRAY_BUFFER, nodes.size() * sizeof(float), nodes.data(), GL_STATIC_DRAW);
		glLineWidth(2.f);
		glDrawArrays(GL_LINES, 0, 2);
		glPointSize(5.f);
		glDrawArrays(GL_POINTS, 0, 1);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

/*INPUT handle*/
bool ArmDesigner::input_handle_() {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		ImGui_ImplSDL2_ProcessEvent(&event);
		//If IMGUI wants the mouse, we skip
		if (ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard) {
			gui_params_.mindist_en = false;
			return true;
		}
		switch (event.type) {
		case SDL_QUIT:
			return false;
		case SDL_MOUSEWHEEL:
		{
			float mouse_x = ImGui::GetIO().MousePos[0] / static_cast<float>(WIDTH);
			float mouse_y = (HEIGHT - ImGui::GetIO().MousePos[1]) / static_cast<float>(HEIGHT);
			float scale = std::abs(render_params_.zoffset) * 2.f * std::tan(glm::radians(render_params_.fov / 2.f));
			if (mouse_x < 0.f || mouse_x > 1.f || mouse_y < 0.f || mouse_y > 1.f) {
				break;
			}
			render_params_.offset.x -= (mouse_x - 0.5) * scale;
			render_params_.offset.y -= (mouse_y - 0.5) * scale;
			SDL_WarpMouseInWindow(infra_.window(), WIDTH / 2, HEIGHT / 2);
			render_params_.fov -= event.wheel.y;
			if (render_params_.fov > 90.f) {
				render_params_.fov = 90.f;
			}
			if (render_params_.fov < 0.2f) {
				render_params_.fov = 0.2f;
			}
			break;
		}
		case SDL_MOUSEMOTION:
		{
			if (gui_params_.selection_mode == SEL_MODE::NONE) {
				gui_params_.mindist_en = true;
				break;
			}

			auto IO = ImGui::GetIO();
			if (IO.KeyShift && IO.MouseDown[0] && gui_params_.selection_mode == SEL_MODE::NODE) {//We're actually dragging the selection (only for nodes)
				if (selected_nodes_.size() > 0) {//actually have a selection to drag
					for (size_t i = 0; i < selected_nodes_.size(); ++i) {
						auto curr_mouse = get_real_mouse_coords_();
						std::array<float, 2> new_pos = {selected_nodes_inipos_[i][0] + curr_mouse[0] - mouse_press_loc_[0], selected_nodes_inipos_[i][1] + curr_mouse[1] - mouse_press_loc_[1] };
						segments_.get_segments()[selected_nodes_[i].first].move_to(selected_nodes_[i].second, new_pos);
					}
				}
			}

			/*Find node with minimal distance (or the one that appears first for equal distances)*/
			auto mouse_coords = get_real_mouse_coords_();
			if (gui_params_.selection_mode == SEL_MODE::NODE) {
				auto tup = find_closest_seg_node_dist_(mouse_coords[0], mouse_coords[1]);
				float tol = 0.01 * std::abs(render_params_.zoffset) * 2.f * /*1000.f **/ std::tan(glm::radians(render_params_.fov / 2.f)); //1 percent of the horizontal view
				if (std::get<2>(tup) <= tol) { //found point within tolerance
					gui_params_.mindist_en = true;
					gui_params_.mindist_seg_i = std::get<0>(tup);
					gui_params_.mindist_node_i = std::get<1>(tup);
					gui_params_.mindist_dist = std::get<2>(tup);
				}
				else { //no point within tolerance
					gui_params_.mindist_en = false;
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::LINE) {
				auto tup = find_closest_seg_line_dist_(mouse_coords[0], mouse_coords[1]);
				float tol = 0.01 * std::abs(render_params_.zoffset) * 2.f * /*1000.f **/ std::tan(glm::radians(render_params_.fov / 2.f)); //1 percent of the horizontal view
				if (std::get<3>(tup) >= 0.f && std::get<3>(tup) <= tol && gui_params_.mindist_seg_i != 0) {//note that we don't allow sketching selections here
					gui_params_.mindist_en = true;
					gui_params_.mindist_seg_i = std::get<0>(tup);
					gui_params_.mindist_node_i = std::get<1>(tup);
					gui_params_.mindist_node_i2 = std::get<2>(tup);
					gui_params_.mindist_dist = std::get<3>(tup);
				}
				else {
					gui_params_.mindist_en = false;
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::SEGMENT) {
				auto tup_node = find_closest_seg_node_dist_(mouse_coords[0], mouse_coords[1]);
				auto tup_line = find_closest_seg_line_dist_(mouse_coords[0], mouse_coords[1]);
				float tol = 0.01 * std::abs(render_params_.zoffset) * 2.f * /*1000.f **/ std::tan(glm::radians(render_params_.fov / 2.f)); //1 percent of the horizontal view
				if (std::get<3>(tup_line) >= 0.f && std::get<3>(tup_line) <= tol) {
					gui_params_.mindist_en = true;
					gui_params_.mindist_seg_i = std::get<0>(tup_line);
					gui_params_.mindist_node_i = std::get<1>(tup_line);
					gui_params_.mindist_node_i2 = std::get<2>(tup_line);
					gui_params_.mindist_dist = std::get<3>(tup_line);
				}
				else if (std::get<2>(tup_node) <= tol) {
					gui_params_.mindist_en = true;
					gui_params_.mindist_seg_i = std::get<0>(tup_node);
					gui_params_.mindist_node_i = std::get<1>(tup_node);
					gui_params_.mindist_dist = std::get<2>(tup_node);
				}
				else {
					gui_params_.mindist_en = false;
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::MEASURE) {
				if (meas_valid_) {
					meas_dist_ = distance(mouse_coords[0], mouse_coords[1], meas_origin_[0], meas_origin_[1]);
				}
			}
			break;
		}
		case SDL_MOUSEBUTTONDOWN:
		{
			auto IO = ImGui::GetIO();
			mouse_press_loc_ = get_real_mouse_coords_();//update click location
			//If we're sketching, add a sketch point and return (continue, really)
			if (gui_params_.selection_mode == SEL_MODE::SKETCH) {
				size_t index = segments_.get_segments()[0].nodes().size();
				segments_.get_segments()[0].insert(index, mouse_press_loc_);
				continue;
			}
			//If shift is pressed, we're moving the selection and skip this step
			if (IO.KeyShift) {
				selected_nodes_inipos_.resize(0);
				selected_nodes_inipos_.reserve(selected_nodes_.size());
				for (auto n : selected_nodes_) {
					selected_nodes_inipos_.push_back(segments_.get_segments()[n.first].nodes()[n.second]);
				}
				continue;
			}
			//If selecting, add closest node to selection
			if (gui_params_.selection_mode == SEL_MODE::NODE) {
				if (IO.KeyCtrl) { //add to selection
					if (gui_params_.mindist_en) {
						bool handled = false;
						//Check if we're deselecting
						for (auto it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
							if (it->first == gui_params_.mindist_seg_i && it->second == gui_params_.mindist_node_i) {
								handled = true;
								selected_nodes_.erase(it);
								break;
							}
						}
						if (!handled) {
							selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
						}
					}
				}
				else {//clear selection & new addition
					selected_nodes_.clear();
					if (gui_params_.mindist_en) {
						selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
					}
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::LINE) {
				if (IO.KeyCtrl) { //add to selection
					if (gui_params_.mindist_en) {
						bool handled = false;
						//Check if we're deselecting
						for (auto it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
							if (it->first == gui_params_.mindist_seg_i && it->second == gui_params_.mindist_node_i) {
								handled = true;
								selected_nodes_.erase(it);
								break;
							}
						}
						if (!handled) {
							selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
						}
					}
				}
				else {//clear selection & new addition
					selected_nodes_.clear();
					if (gui_params_.mindist_en) {
						selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
					}
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::SEGMENT) {
				if (IO.KeyCtrl) {
					if (gui_params_.mindist_en) {
						bool handled = false;
						//Check if we're deselecting
						for (auto it = selected_nodes_.begin(); it != selected_nodes_.end(); ++it) {
							if (it->first == gui_params_.mindist_seg_i) {
								handled = true;
								selected_nodes_.erase(it);
								break;
							}
						}
						if (!handled) {
							selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
						}
					}
				}
				else {//clear selection & new addition
					selected_nodes_.clear();
					if (gui_params_.mindist_en) {
						selected_nodes_.push_back({ gui_params_.mindist_seg_i, gui_params_.mindist_node_i });
					}
				}
			}
			else if (gui_params_.selection_mode == SEL_MODE::MEASURE) {
				gui_params_.log_messages.push_back("Setting measurement origin at (" + std::to_string(mouse_press_loc_[0]) + ", " + std::to_string(mouse_press_loc_[1]) + ").");
				meas_origin_ = mouse_press_loc_;
				meas_dist_ = 0.f;
				meas_valid_ = true;
			}
			break;
		}
		case SDL_KEYDOWN:
		{
			//key presses
			/*
				M		Add midpoint node to each selected line
				S		Split segment between two nodes
				A		Append a node to the end of the segment
				P		Prepend a node to the beginning of the segment
				D		Delete selected nodes
				ESC	Clear selection
			*/
			if (event.key.keysym.sym == SDLK_m) {//m
				if (selected_nodes_.size() == 0 || selected_nodes_[0].first == 0) {
					gui_params_.log_messages.push_back("Sketch segment doesn't allow midpointing.");
					continue;
				}
				if (gui_params_.selection_mode == SEL_MODE::LINE){//only in line selection mode
					gui_params_.log_messages.push_back("Splitting " + std::to_string(selected_nodes_.size()) + " lines.");
					//split all selected lines
					//first sort the selected segments
					std::sort(selected_nodes_.begin(), selected_nodes_.end(), [](auto a, auto b) {return a.second < b.second; });
					std::stable_sort(selected_nodes_.begin(), selected_nodes_.end(), [](auto a, auto b) {return a.first < b.first; });
					std::vector < std::pair<size_t, size_t> > new_selection;
					new_selection.reserve(2 * selected_nodes_.size());
					std::vector<size_t> seg_increments(segments_.get_segments().size(), 0);
					for (size_t i = 0; i < selected_nodes_.size(); ++i) {
						auto node1 = segments_.get_segments()[selected_nodes_[i].first].nodes()[selected_nodes_[i].second + seg_increments[selected_nodes_[i].first]];
						auto node2 = segments_.get_segments()[selected_nodes_[i].first].nodes()[selected_nodes_[i].second + seg_increments[selected_nodes_[i].first] + 1];
						std::array<float, 2> new_node{ (node1[0] + node2[0]) / 2.f, (node1[1] + node2[1]) / 2.f };
						segments_.get_segments()[selected_nodes_[i].first].insert(selected_nodes_[i].second + seg_increments[selected_nodes_[i].first] + 1, new_node);
						new_selection.push_back({ selected_nodes_[i].first, selected_nodes_[i].second + seg_increments[selected_nodes_[i].first] });
						new_selection.push_back({ selected_nodes_[i].first, selected_nodes_[i].second + seg_increments[selected_nodes_[i].first] + 1 });
						seg_increments[selected_nodes_[i].first]++;
					}
					selected_nodes_ = new_selection;
				}
				else {
					gui_params_.log_messages.push_back("Only lines can be split.");
				}
			}
			else if (event.key.keysym.sym == SDLK_s) {//s
				if (selected_nodes_.size() != 2 || selected_nodes_[0].first != selected_nodes_[1].first) {
					gui_params_.log_messages.push_back("Splitting not possible: Exactly two nodes of the same segment need to be selected.");
					continue;
				}
				else if (gui_params_.selection_mode != SEL_MODE::NODE) {
					gui_params_.log_messages.push_back("Splitting only possible in node selection mode.");
					continue;
				}
				else if (selected_nodes_[0].first == 0) {
					gui_params_.log_messages.push_back("Sketch segment doesn't allow splitting.");
					continue;
				}
				else {
					gui_params_.log_messages.push_back("Splitting Segment.");
					segments_.split(selected_nodes_[0].first, selected_nodes_[0].second, selected_nodes_[1].second);
					gui_params_.segments_shown.push_back(true);
					gui_params_.segments_repetitions.push_back(gui_params_.segments_repetitions[selected_nodes_[0].first]);
					gui_params_.segments_grayscale.push_back(false);
					selected_nodes_.clear();
				}
			}
			else if (event.key.keysym.sym == SDLK_a) {//a
				if (selected_nodes_.size() != 1) {
					gui_params_.log_messages.push_back("Appending only possible for 1 selected segment.");
					continue;
				}
				else if (gui_params_.selection_mode != SEL_MODE::SEGMENT) {
					gui_params_.log_messages.push_back("Appending only possible in segment selection mode.");
					continue;
				}
				else if (selected_nodes_[0].first == 0) {
					gui_params_.log_messages.push_back("Sketch segment doesn't allow appending.");
					continue;
				}
				else {
					gui_params_.log_messages.push_back("Appending Node.");
					auto node1 = segments_.get_segments()[selected_nodes_[0].first].nodes().back();
					auto node2 = segments_.get_segments()[selected_nodes_[0].first].nodes().front();
					std::array<float, 2> new_node{ (node1[0] + node2[0]) / 2.f, (node1[1] + node2[1]) / 2.f };
					auto index = segments_.get_segments()[selected_nodes_[0].first].nodes().size();
					segments_.get_segments()[selected_nodes_[0].first].insert(index, new_node);
				}
			}
			else if (event.key.keysym.sym == SDLK_p) {//p
				if (selected_nodes_.size() != 1) {
					gui_params_.log_messages.push_back("Prepending only possible for 1 selected segment.");
					continue;
				}
				else if (gui_params_.selection_mode != SEL_MODE::SEGMENT) {
					gui_params_.log_messages.push_back("Prepending only possible in segment selection mode.");
					continue;
				}
				else if (selected_nodes_[0].first == 0) {
					gui_params_.log_messages.push_back("Sketch segment doesn't allow prepending.");
					continue;
				}
				else {
					gui_params_.log_messages.push_back("Prepending Node.");
					auto node1 = segments_.get_segments()[selected_nodes_[0].first].nodes().back();
					auto node2 = segments_.get_segments()[selected_nodes_[0].first].nodes().front();
					std::array<float, 2> new_node{ (node1[0] + node2[0]) / 2.f, (node1[1] + node2[1]) / 2.f };
					segments_.get_segments()[selected_nodes_[0].first].insert(0, new_node);
				}
			}
			else if (event.key.keysym.sym == SDLK_d) {
				if (gui_params_.selection_mode != SEL_MODE::NODE && gui_params_.selection_mode != SEL_MODE::SEGMENT) {
					gui_params_.log_messages.push_back("Only nodes and segments can be deleted.");
					continue;
				}
				else if (selected_nodes_.size() == 0) {
					continue;
				}
				else {
					bool issue = false;
					for (auto n : selected_nodes_) {
						if (n.first != selected_nodes_[0].first) {
							issue = true;
							break;
						}
					}
					if (issue) {
						gui_params_.log_messages.push_back("Only single-segment deletions are supported.");
						continue;
					}
					else if (gui_params_.selection_mode != SEL_MODE::SEGMENT && selected_nodes_.size() >= segments_.get_segments()[selected_nodes_[0].first].nodes().size()) {
						gui_params_.log_messages.push_back("Can't delete whole segments in node deletion mode.");
						continue;
					}
					if (gui_params_.selection_mode == SEL_MODE::NODE) {
						gui_params_.log_messages.push_back("Deleting " + std::to_string(selected_nodes_.size()) + " nodes.");
						//collect coordinates
						std::vector<std::array<float, 2> > coords;
						coords.reserve(selected_nodes_.size());
						for (auto n : selected_nodes_) {
							coords.push_back(segments_.get_segments()[n.first].nodes()[n.second]);
						}
						segments_.get_segments()[selected_nodes_[0].first].erase(coords);
					}
					else if (gui_params_.selection_mode == SEL_MODE::SEGMENT) {
						if (selected_nodes_.size() != 1) {
							gui_params_.log_messages.push_back("Only single segment deletions supported.");
							continue;
						}
						if (selected_nodes_[0].first == 0) {
							gui_params_.log_messages.push_back("Clearing sketch.");
							segments_.get_segments()[0].clear();
							continue;
						}
						else {
							gui_params_.log_messages.push_back("Deleting segment.");
							segments_.erase(selected_nodes_[0].first);
							gui_params_.segments_shown.erase(gui_params_.segments_shown.begin() + selected_nodes_[0].first);
							gui_params_.segments_repetitions.erase(gui_params_.segments_repetitions.begin() + selected_nodes_[0].first);
							gui_params_.segments_grayscale.erase(gui_params_.segments_grayscale.begin() + selected_nodes_[0].first);
						}
					}
					selected_nodes_.clear();
					gui_params_.mindist_en = false;

				}
			}
			else if (event.key.keysym.sym == SDLK_c) {//c
				//check that no sketch nodes are selected
				bool issue = false;
				for (auto n : selected_nodes_) {
					if (n.first == 0) {
						issue = true;
						gui_params_.log_messages.push_back("Cannot copy sketch nodes.");
						break;
					}
				}
				if (issue) {
					continue;
				}
				else {
					//copy all nodes to sketch layer
					size_t inserted = 0;
					for (auto n : selected_nodes_) {
						//TODO: Should really insert at the end for performance, but cba right now.
						segments_.get_segments()[0].insert(0, segments_.get_segments()[n.first].nodes()[n.second]);
						inserted++;
					}
					//change selection to the new nodes
					selected_nodes_.clear();
					for (size_t i = 0; i < inserted; ++i) {
						selected_nodes_.push_back({0, i});
					}
					gui_params_.log_messages.push_back("Copied " + std::to_string(inserted) + " nodes to sketch.");
				}
			}
			else if (event.key.keysym.sym == SDLK_ESCAPE) {//esc
				//clear selection
				gui_params_.log_messages.push_back("Deselect registered");
				selected_nodes_.clear();
				meas_valid_ = false;
			}
			break;
		}
		}

	}
	return true;
}

void ArmDesigner::reset_view_() {
	gui_params_.log_messages.push_back("Resetting View");
	render_params_.fov = 60;
	render_params_.offset.x = 0.f;
	render_params_.offset.y = 0.f;
}


/*Find closest segment and node*/
std::tuple<size_t, size_t, float> ArmDesigner::find_closest_seg_node_dist_(const float x, const float y) const{
	auto segs = segments_.get_segments();

	float min_dist = 999999.;
	size_t min_seg_i = 99999;
	size_t min_node_i = 99999;
	for (size_t i = 0; i < segs.size(); ++i) {
		if(!gui_params_.segments_shown[i] || gui_params_.segments_grayscale[i]){ //Don't consider hidden segments
			continue;
		}
		auto nodes = segs[i].nodes();
		for (size_t k = 0; k < nodes.size(); ++k) {
			float dist = distance(x, y, nodes[k][0], nodes[k][1]);
			if (dist < min_dist) {
				min_dist = dist;
				min_seg_i = i;
				min_node_i = k;
			}
		}
	}
	return { min_seg_i, min_node_i, min_dist };
}

/*Find closest segment, line and distance*/
/*Positive distance means result is valid*/
std::tuple<size_t, size_t, size_t, float> ArmDesigner::find_closest_seg_line_dist_(const float x, const float y) const{
	auto segs = segments_.get_segments();

	float min_dist = 999999.;
	size_t min_seg_i = 10 * segs.size(); /*if min_seg_i > segs.size, nothing was found*/
	size_t min_node_i = 99999;
	for (size_t i = 0; i < segs.size(); ++i){
		if(!gui_params_.segments_shown[i] || gui_params_.segments_grayscale[i]){
			continue;
		}
		auto nodes = segs[i].nodes();
		if (nodes.size() == 0) {//fix segmentation fault
			continue;
		}
		for (size_t k = 0; k < nodes.size()-1; ++k){
			/*Find direction and normal vector of line*/
			std::array<float, 2> dirv { nodes[k+1][0] - nodes[k][0], nodes[k+1][1] - nodes[k][1] }; //rg
			std::array<float, 2> norv { -dirv[1], dirv[0] }; //rh
			/*Normalize*/
			float dirvlen = std::sqrt(dirv[0] * dirv[0] + dirv[1] * dirv[1]);
			assert(dirvlen > 0.f, "Nodes are on top of each other, zero distance between them");
			float xg = nodes[k][0];
			float yg = nodes[k][1];
			float xh = x;
			float yh = y;
			/*Calculate crossing*/
			float s = (yg - yh + (xh - xg) * dirv[1] / dirv[0]) / (norv[1] - (norv[0] * dirv[1]) / (dirv[0]));
			float t = (xh - xg + s * norv[0]) / dirv[0];
			/*Check if we're straight above*/
			if (t < 1.f && t > 0.f) {
				if (std::abs(s) * dirvlen < min_dist) {
					min_dist = std::abs(s) * dirvlen;
					min_seg_i = i;
					min_node_i = k;
				}
			}
		}
	}
	if (min_seg_i > segs.size()) {
		return { min_seg_i, min_node_i, min_node_i, -1.f };
	}
	else {
		return { min_seg_i, min_node_i, min_node_i + 1, min_dist };
	}
}

/*Translate mouse coords into real space coords*/
std::array<float, 2> ArmDesigner::get_real_mouse_coords_() const{
	float mouse_x = ImGui::GetIO().MousePos[0] / static_cast<float>(WIDTH);
	float mouse_y = (HEIGHT - ImGui::GetIO().MousePos[1]) / static_cast<float>(HEIGHT);
	float scale = std::abs(render_params_.zoffset) * 2.f * std::tan(glm::radians(render_params_.fov / 2.f));
	float mx = -(render_params_.offset.x - (mouse_x - 0.5) * scale);
	float my = -(render_params_.offset.y - (mouse_y - 0.5) * scale);
	return { mx, my };
}
